/* CREDITS
Form code: https://www.w3resource.com/javascript/form/javascript-sample-registration-form-validation.php
Birthday validation code: https://www.aspsnippets.com/Articles/Date-of-Birth-Age-Validation-in-JavaScript.aspx */

function formValidation() {
	var uname = document.registration.username;
	var uadd = document.registration.address;
	var uemail = document.registration.email;
	var passid = document.registration.passid;
	var confpw = document.registration.confpass;
	var telno = document.registration.telno;
	var ubday = document.registration.bday;


	if (allLetter(uname)) {
		if (alphanumeric(uadd)) {
			if (ValidateEmail(uemail)) {
				if (passid_validation(passid, 7, 12)) {
					if (conf_passid(passid, confpw)) {
						if (tel_validation(telno)) {
							if (bday_validation(ubday)) {}
						}
					}
				}
			}
		}
	}
	return false;
}

//username validation
function allLetter(uname) {
	var letters = /^[A-Za-z]+$/;
	if (uname.value.match(letters)) {
		return true;
	} else {
		alert('Username must have alphabet characters only');
		uname.focus();
		return false;
	}
}

//address validation
function alphanumeric(uadd) {
	var letters = /^[0-9a-zA-Z]+$/;
	if (uadd.value.match(letters)) {
		return true;
	} else {
		alert('User address must have alphanumeric characters only');
		uadd.focus();
		return false;
	}
}

//email validation
function ValidateEmail(uemail) {
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (uemail.value.match(mailformat)) {
		return true;
	} else {
		alert("You have entered an invalid email address!");
		uemail.focus();
		return false;
	}
}

//password validation
function passid_validation(passid, mx, my) {
	var passid_len = passid.value.length;
	if (passid_len == 0 || passid_len >= my || passid_len < mx) {
		alert("Password should not be empty / length should be between " + mx + " to " + my);
		passid.focus();
		return false;
	}
	return true;
}

//password confirmation validation
function conf_passid(passid, confpw) {
	if (passid.value.match(confpw.value)) {
		return true;
	} else {
		alert("Password should match");
		passid.focus();
		return false;
	}
}

//phone number validation
function tel_validation(tel) {
	var phoneno = /^\d{10}$/;
	if ((tel.value.match(phoneno))) {
		return true;
	} else {
		alert("Phone number should be 10 digits including area code");
		return false;
	}
}

//birthday validation
function bday_validation(ubday) {
	var dateString = ubday.value;
	var dob = /(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$/;

	if (dob.test(dateString)) {
		var parts = dateString.split("/");
		var dtDOB = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
		var dtCurrent = new Date();
		if (dtCurrent.getFullYear() - dtDOB.getFullYear() < 18) {
			alert("Eligibility must be over 18 years");
			return false;
		}
		//over 18 validation
		if (dtCurrent.getFullYear() - dtDOB.getFullYear() == 18) {
			if (dtCurrent.getMonth() < dtDOB.getMonth()) {
				alert("Eligibility must be over 18 years");
				return false;
			}
			if (dtCurrent.getMonth() == dtDOB.getMonth()) {
				if (dtCurrent.getDate() < dtDOB.getDate()) {
					alert("Eligibility must be over 18 years");
					return false;
				}
			}
		}
		if (dtCurrent.getFullYear() - dtDOB.getFullYear() > 50) {
			alert("Over 50 please visit this link");
			return false;
		}

		//over 50 message
		if (dtCurrent.getFullYear() - dtDOB.getFullYear() == 50) {
			if (dtCurrent.getMonth() < dtDOB.getMonth()) {
				alert("Over 50 please visit this link");
				return false;
			}
			if (dtCurrent.getMonth() == dtDOB.getMonth()) {
				if (dtCurrent.getDate() < dtDOB.getDate()) {
					alert("Over 50 please visit this link");
					return false;
				}
			}
		}
	}
	if ((!ubday.value.match(dob))) {
		alert("Enter date in dd/MM/yyyy format ONLY.");
		return false;
	} else {
		alert('Form Succesfully Submitted');
		window.location.reload()
		return true;
	}
}
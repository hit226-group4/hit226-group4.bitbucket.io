// Wait for the page to finish loading
document.addEventListener('DOMContentLoaded', function () {
    try {
        document.getElementById('bday').addEventListener('input', ageValidation);
        document.getElementById('mobileNumber').addEventListener('input', mobileValidate);
        document.getElementById('password2').addEventListener('input', passwordValidate);
        document.getElementById('password').addEventListener('input', passwordStrength);

        setMaxBday()
    } catch (error) {
        
    }

    const cards = Object.values(document.getElementsByClassName('card-showDescription'));

    cards.forEach(card => {
        card.addEventListener('click', showCardDescription);
    });

    const increaseButtons = Object.values(document.getElementsByClassName('increase'));

    increaseButtons.forEach(button => {
        button.addEventListener('click', increaseQuantity);
    });

    const decreaseButtons = Object.values(document.getElementsByClassName('decrease'));

    decreaseButtons.forEach(button => {
        button.addEventListener('click', decreaseQuantity);
    });

  }, false);



/**
 * Card Methods
 */

function showCardDescription (e) {

    var description = this.nextElementSibling;
    var currentDisplay = getStyle(description)

    if (currentDisplay == "block") {
        description.style.display  = "none";
    } else if (currentDisplay == "none") {
        description.style.display = "block";
    } 

}

function decreaseQuantity () {

    if (this.previousElementSibling.value > 0) {
        this.previousElementSibling.value --
    } else {
        
    }


}

function increaseQuantity () {
    this.previousElementSibling.previousElementSibling.value ++
}


function getStyle (element) {

    if (!element.style.display) {
        console.log("hi")
        return window.getComputedStyle(element, null).getPropertyValue("display")
    } else {
        return element.style.display
    }
}

/**
 * Google AutoComplete API
 */


let autocomplete;
function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(document.getElementById('street'),
        {
            componentRestrictions: {'country':['AU']},
            fields: ["address_components"]
        }
    );
    
    try {
        autocomplete.addListener('place_changed', userSelectedPlace)
    } catch (error) {
        
    }
}

function userSelectedPlace() {

    var addressComponents = autocomplete.getPlace().address_components;
    console.log(addressComponents);
    if(!addressComponents) {
        console.log('No place found')
    } else {

        addressFields = {
            "subpremise" : "",
            "street_number" : "",
            'route': "",
            'locality' : "",
            "administrative_area_level_1" : "",
            'postal_code' : "",
            "country" : "",
        }

        for (const adrComp of addressComponents) {
            console.log(adrComp)
                addressFields[adrComp.types[0]] = adrComp.short_name
        }

            
            document.getElementById("street").value = addressFields["subpremise"] + " " + addressFields["street_number"] + " " + addressFields["route"];
            document.getElementById("postCode").value = addressFields["postal_code"] ;
            document.getElementById("suburb").value = addressFields["locality"] ;
            document.getElementById("state").value = addressFields["administrative_area_level_1"] ;
            document.getElementById("country").value = addressFields["country"] ; 
    }

}

/**
 * Form Validation
 */

//Restrict birthday field to allow dates equal to today or earlier.
function setMaxBday() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    
    if (dd < 10) {
       dd = '0' + dd;
    }
    
    if (mm < 10) {
       mm = '0' + mm;
    } 
        
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("bday").setAttribute("max", today);
}

function ageValidation(e) {

    var selectedDate = new Date(e.target.value).getTime();
    var todayDate = Date.now();
    var dateDiff = new Date(todayDate - selectedDate);
    var ageYears = Math.abs(dateDiff.getUTCFullYear() - 1970);

    //Remove previously added content.
    try {
        document.getElementById('under13').remove()
        document.getElementById('under13label').remove()
    } catch {}

    try {
        document.getElementById('ageHelpLink').remove()
    } catch {}

    if (ageYears < 13) {

        document.getElementById('bdayGroup').insertAdjacentHTML('afterend',
        '<input type="checkbox" id="under13" name="under13" required></input>'
        );
        
        document.getElementById('bdayGroup').insertAdjacentHTML('afterend',
            '<label for="under13" id ="under13label">Under 13, confirm parental supervision.</label>'
        );        

    } else if (ageYears > 51) {
        document.getElementById('bdayGroup').insertAdjacentHTML('afterend',
        '<a href="#" id ="ageHelpLink">Get assistance with this page.</a>'
        );
        
    } else {}

}

 function mobileValidate(e) { 

    if (e.target.validity.patternMismatch) {
        e.target.setCustomValidity("Please enter a valid 10 digit phone number.");
        e.target.reportValidity();
      } else {
        e.target.setCustomValidity("");
      }
 }

 function passwordValidate(e) { 

    var firstPassword = document.getElementById('password').value;

    if (e.target.value != firstPassword) {
        e.target.setCustomValidity("Both passwords must match!");
        e.target.reportValidity();
      } else {
        e.target.setCustomValidity("");
      }
      
 }


 function passwordStrength(e) { 

    var password = document.getElementById('password').value;

    //Define strength characteristics using regEx

    var regChecks = 
    [
        new RegExp('(?=.{8,})'), //8 or more characters
        new RegExp('(?=.*[A-Z])'), //1 or more capital letters
        new RegExp('([^A-Za-z0-9])'), //1 or more numbers
        new RegExp('(?=.*[0-9])') //1 or more special characters
    ];

    var passwordSecurityScore = 0;

    for (const regEx of regChecks) {
        if (regEx.test(password)) {passwordSecurityScore += 1}
    }

    var passwordSecurityStrength = "Very Weak"
    switch (passwordSecurityScore) {
        case 1:
            passwordSecurityStrength = "Weak";
            break;
        case 2:
            passwordSecurityStrength = "Medium";
            break;
        case 3:
            passwordSecurityStrength = "Strong";
            break;
        case 4:
            passwordSecurityStrength = "Very Strong";
            break;
    }

    document.getElementById('passwordStrength').innerHTML = "Strength: " + passwordSecurityStrength;
     
 }
